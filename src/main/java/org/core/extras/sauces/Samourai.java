package org.core.extras.sauces;

import org.core.extras.ExtraDecorator;
import org.core.foods.Food;

public class Samourai extends ExtraDecorator {

    public Samourai(Food food) {
        wrappedFood = food;
    }

    @Override
    public String getDescription() {
        return wrappedFood.getDescription() + " + Samouraï";
    }

    @Override
    public double cost() {
        return wrappedFood.cost() + .1;
    }
}
