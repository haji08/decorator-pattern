package org.core.extras.sauces;

import org.core.extras.ExtraDecorator;
import org.core.foods.Food;

public class Ketchup extends ExtraDecorator {

    public Ketchup(Food food) {
        wrappedFood = food;
    }

    @Override
    public String getDescription() {
        return wrappedFood.getDescription() + " + Ketchup";
    }

    @Override
    public double cost() {
        return wrappedFood.cost() + .1;
    }
}
