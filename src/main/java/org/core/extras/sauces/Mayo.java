package org.core.extras.sauces;

import org.core.extras.ExtraDecorator;
import org.core.foods.Food;

public class Mayo extends ExtraDecorator {

    public Mayo(Food food) {
        wrappedFood = food;
    }

    @Override
    public String getDescription() {
        return wrappedFood.getDescription() + " + Mayo";
    }

    @Override
    public double cost() {
        return wrappedFood.cost() + .1;
    }
}
