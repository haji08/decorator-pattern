package org.core.extras.sauces;

import org.core.extras.ExtraDecorator;
import org.core.foods.Food;

public class Biggy extends ExtraDecorator {

    public Biggy(Food food) {
        this.wrappedFood = food;
    }

    @Override
    public String getDescription() {
        return wrappedFood.getDescription() + " + Biggy";
    }

    @Override
    public double cost() {
        return wrappedFood.cost() + .1;
    }
}
