package org.core.extras.meats;

import org.core.extras.ExtraDecorator;
import org.core.foods.Food;

public class Kebab extends ExtraDecorator {

    public Kebab(Food food) {
        this.wrappedFood = food;
    }

    @Override
    public String getDescription() {
        return wrappedFood.getDescription() + " + Kebab";
    }

    @Override
    public double cost() {
        return wrappedFood.cost() + 1.3;
    }
}
