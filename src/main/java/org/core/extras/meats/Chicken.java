package org.core.extras.meats;

import org.core.extras.ExtraDecorator;
import org.core.foods.Food;

public class Chicken extends ExtraDecorator {

    public Chicken(Food food) {
        wrappedFood = food;
    }

    @Override
    public String getDescription() {
        return wrappedFood.getDescription() + " + Chicken";
    }

    @Override
    public double cost() {
        return wrappedFood.cost() + .98;
    }
}
