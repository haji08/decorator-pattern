package org.core.extras.meats;

import org.core.extras.ExtraDecorator;
import org.core.foods.Food;

public class Nuggets extends ExtraDecorator {

    public Nuggets(Food food) {
        wrappedFood = food;
    }

    @Override
    public String getDescription() {
        return wrappedFood.getDescription() + " + Nuggets";
    }

    @Override
    public double cost() {
        return wrappedFood.cost() + 1.2;
    }
}
