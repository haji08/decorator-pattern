package org.core.extras.meats;

import org.core.extras.ExtraDecorator;
import org.core.foods.Food;

public class MincedMeat extends ExtraDecorator {

    public MincedMeat(Food food) {
        wrappedFood = food;
    }

    @Override
    public String getDescription() {
        return wrappedFood.getDescription() + " + Minced Meat";
    }

    @Override
    public double cost() {
        return wrappedFood.cost() + 1.5;
    }
}
