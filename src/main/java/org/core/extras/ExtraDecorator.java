package org.core.extras;

import org.core.foods.Food;

/**
 * @author hadji08
 * Here we’re using inheritance to achieve the type matching, but we
 * aren’t using inheritance to get behavior.
 * The decorators must have the same type as the objects
 * they are going to decorate
 */
public abstract class ExtraDecorator extends Food {
    protected Food wrappedFood;

    public abstract String getDescription();
}
