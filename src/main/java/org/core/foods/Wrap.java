package org.core.foods;

public class Wrap extends Food {
    @Override
    public String getDescription() {
        return "Wrap";
    }

    @Override
    public double cost() {
        return 9.0;
    }
}
