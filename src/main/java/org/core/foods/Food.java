package org.core.foods;

/**
 * @author hadji08
 */
public abstract class Food {
    public abstract String getDescription();

    public abstract double cost();


    @Override
    public String toString() {
        return String.format("%s $%.2f", getDescription(), cost());
    }
}
