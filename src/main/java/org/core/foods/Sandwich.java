package org.core.foods;

public class Sandwich extends Food {
    @Override
    public String getDescription() {
        return "Sandwich";
    }

    @Override
    public double cost() {
        return 7.5;
    }
}
