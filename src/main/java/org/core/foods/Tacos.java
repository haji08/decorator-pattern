package org.core.foods;

public class Tacos extends Food {
    @Override
    public String getDescription() {
        return "Tacos";
    }

    @Override
    public double cost() {
        return 6.0;
    }
}
