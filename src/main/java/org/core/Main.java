package org.core;

import org.core.extras.meats.Chicken;
import org.core.extras.meats.Kebab;
import org.core.extras.meats.MincedMeat;
import org.core.extras.meats.Nuggets;
import org.core.extras.sauces.Ketchup;
import org.core.extras.sauces.Mayo;
import org.core.extras.sauces.Samourai;
import org.core.foods.Food;
import org.core.foods.Tacos;
import org.core.foods.Wrap;

public class Main {
    public static void main(String[] args) {

        Food tacos = new Mayo(new Samourai(new MincedMeat(new Chicken(new Tacos()))));

        System.out.println(tacos);

        Food wrap = new Ketchup(new Samourai(new Kebab(new Nuggets(new Wrap()))));

        System.out.println(wrap);
    }
}