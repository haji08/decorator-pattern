# Decorator pattern implementation example

The Decorator Pattern attaches additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.

## Contributors
- [@haji08](https://gitlab.com/haji08)
